package api;

import api.core.definition.ConstructAPI;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class ApiDefinitions {
    @Steps
    ConstructAPI constructAPI;

    @Given("^I send a request with postcode (.*)$")
    public void i_send_a_get_request_to_api(String postcode) {
            assertNotNull(constructAPI.testPostCode(postcode));
    }

    @Then("I get a {int} response")
    public void i_get_a_response(int responseCode) {
        int actualResponseCode = constructAPI.getResponseStatusCode();
        assertThat(responseCode, is(actualResponseCode));
    }
}




