package api.core.endpoint;


public interface PostcodeHost {
    String hostname();
    default Schema schema(){return Schema.HTTP;}
    default int port(){return schema().getPort();}
}
