package api.core.endpoint;

public enum Schema {
    HTTP(80), HTTPS(443);
    private final int port;

    Schema(int port) {
        this.port = port;
    }

    public int getPort() {
        return port;
    }
}
