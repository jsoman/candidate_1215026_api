package api.core.definition;

import api.core.endpoint.PostcodeHost;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class ConstructAPI {
    private static final String BASE_URI = "api.postcodes.io/postcodes";
    private Response response;

    @Step
    public Response testPostCode(String postcode) {
        response = SerenityRest.given().get(getUrl(() -> BASE_URI, postcode));
        return response;
    }

    @Step
    public int getResponseStatusCode() {
        return response.statusCode();
    }

    private String getUrl(PostcodeHost postcodeHost, String postcode) {
        String url = String.format("%s://%s/%s",
                postcodeHost.schema(), postcodeHost.hostname(), postcode);
        return url;
    }

}













