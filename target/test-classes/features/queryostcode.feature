Feature:Query a postcode and receive a 200 response

  Scenario Outline: Query a postcode and get correct response
    Given I send a request with postcode <PostCode>
    Then I get a 200 response
    Examples:
    | PostCode |
    | SW1P4JA  |


